const youtubedl = require("youtube-dl")
const ffmpeg = require("fluent-ffmpeg")
const moment = require("moment")
const async = require("async")
const fs = require("fs")

// const config = require("./config.json")
// const argv = require("minimist")(process.argv.slice(2))
const urls = [""]

const worker = async.queue((url, cb) => {
  const playlistMarker = url.match(/playlist/) ? true : false
  const videoFiles = []
  const doPlaylist = url => {
    const video = youtubedl(url)

    video.on("error", function error(err) {
      console.log("doPlylist error:", err)
    })

    let size = 0
    video.on("info", function(info) {
      size = info.size
      const path = info.playlist_title
        ? `${__dirname}/tmp/${info.playlist_title}`
        : `${__dirname}/tmp`

      const output = `${path}/${info._filename}`

      if (!fs.existsSync(path) && info.playlist_title) {
        fs.mkdirSync(path)
      }

      videoFiles.push(output)
      video.pipe(fs.createWriteStream(output))
    })

    let pos = 0
    video.on("data", function data(chunk) {
      pos += chunk.length
      if (size) {
        let percent = ((pos / size) * 100).toFixed(2)
        process.stdout.cursorTo(0)
        process.stdout.clearLine(1)
        process.stdout.write(percent + "%")
      }
    })

    video.on("end", () => {
      convert(videoFiles)
    })
    video.on("next", doPlaylist)
    cb()
  }

  const doVideo = url => {
    const getName = new Promise((resolve, reject) => {
      youtubedl.getInfo(url, (err, info) => {
        if (err) reject(err)
        resolve(info._filename)
      })
    })
    const video = youtubedl(url)

    getName.then(name => {
      const pathToFile = `${__dirname}/tmp/${name}`
      videoFiles.push(pathToFile)
      video.pipe(fs.createWriteStream(pathToFile))
    })

    video.on("end", () => {
      convert(videoFiles)
    })
  }

  const convert = videoFiles => {
    const videoFile = videoFiles[videoFiles.length - 1]

    ffmpeg(videoFile)
      .saveToFile(videoFile.replace(/\.mp4/, ".mp3"))
      .on("end", () => {
        console.log(" Converting done. Deleting:", videoFile)
        logging(
          `${moment().format("L")}  ${videoFile
            .replace(/.+tmp\//, "")
            .replace(/\.mp4/, "")} \n`
        )

        fs.unlinkSync(videoFile)
        !playlistMarker ? cb() : null
      })
  }

  playlistMarker ? doPlaylist(url) : doVideo(url)
})

const logging = logData => {
  const logPath = `${__dirname}/log.txt`

  fs.existsSync(logPath)
    ? fs.appendFileSync(logPath, logData, "utf8")
    : fs.writeFileSync(logPath, logData, "utf8")
}

worker.push(urls)
